import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';
import { title } from 'utils';
import { userService } from 'services';
import { Article } from '../components';
import dateParser from '../utils/dateParser'

class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: null,
            user: {}
        }
        this.handleSelect = this.handleSelect.bind(this)
    }

    componentDidMount() {
        const { id } = this.props.match.params
        userService.list()
            .then((resolve, reject) => {
                this.setState({ list: resolve, id: id ? id : resolve[0].id })
            })
            .then(() => {
                userService.get(this.state.id)
                    .then((resolve, reject) => {
                        this.setState({ user: resolve })
                    })
            })
    }

    componentDidUpdate() {
        if (this.state.id != this.props.match.params.id) {
            this.fetchUser(this.props.match.params.id)
        }

    }

    async fetchUser(id) {
        id = id ? id : this.state.list[0].id
        await userService.get(id)
        .then((resolve, reject) => {
            if(reject){
                this.setState({ id: null, user: {} })
            }
            this.setState({ id,
user: resolve })
        })
    }

    async handleSelect(event) {
        if (event.target.value != this.state.id) {
            await this.props.history.push(`/users/${event.target.value}`)
        }
    }


    render() {
        const users = this.state.list.map(el => {
            return <option key={`userkey-${el.id}`} value={el.id} selected={el.id == this.state.id}>{el.name}</option>})
        const articles = (this.state.user.hasOwnProperty('articles'))
            ? this.state.user.articles.map((el, index) => {return Article(el)})
            : (<div><h2>Cette utilisateur n'existe pas.</h2></div>)
        const date = dateParser(this.state.user.birthdate)

        return (
            <Fragment>
                <Helmet>
                    { title('Page secondaire') }
                </Helmet>

                <div className="user-page content-wrap">
                    <Link to="/" className="nav-arrow">
                        <Icon style={{ transform: 'rotate(180deg)' }}>arrow_right_alt</Icon>
                    </Link>

                    <div className="users-select" >
                        <select onChange={this.handleSelect} defaultValue={this.state.id}>
                            {users}
                        </select>
                    </div>

                    <div className="infos-block">
                        <h2>Occupation: <span className="white-text">{this.state.user.occupation}</span></h2>
                        <h2>Date de naissance: <span className="white-text">{date}</span></h2>
                    </div>

                    <div className="articles-list">
                        {articles}
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default UserPage;
