import React from 'react'

export const Article = props => {
    var name = props.name.split(" ");
    name = `${name[0]} # ${name[1]}`;
    return (
    <div className="article" key={`articlekey-${props.id}`}>
        <h3>{name}</h3>
        <p>{props.content.replace(/"/g, '')}</p>
    </div>
    )

}
